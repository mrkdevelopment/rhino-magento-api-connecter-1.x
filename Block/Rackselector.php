<?php


class RhinoRacks_APIConnector_Block_Rackselector extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface
{

    protected function _toHtml()
    {
        $view = __DIR__ . '/../views/rackselector.php';
        ob_start();
        require $view;
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}
