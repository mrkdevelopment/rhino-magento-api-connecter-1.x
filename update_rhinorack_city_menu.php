<?php

/**
 * Script which stores the 4 static blocks as unordered HTML lists, 2 each for nested rack and accessory subcategories. Useful for create a 'mega menu'
 */
require_once __DIR__ . '/di.php';

$container['clear_cache'];

// Accessories
$container['category_api']->touchCategoryMain();

$children     = $container['category_api']->getChildrenCategories($container['accessories_category']);
$count        = count($children);
$offset       = (int) $count/2;
$accessory_1  = array_slice($children, 0, $offset);
$accessory_2  = array_slice($children, $offset);

$products_3 = $container['accessory_api']->menu_html($accessory_1);
$container['helper']->save_static_block('products_3', $products_3);

$products_4 = $container['accessory_api']->menu_html($accessory_2);
$container['helper']->save_static_block('products_4', $products_4);

// Racks
$container['category_api']->touchRackMain();
$children = $container['category_api']->getChildrenCategories($container['racks_category']);

$count   = count($children);
$offset  = (int) $count/2;
$racks_1 = array_slice($children, 0, $offset);
$racks_2 = array_slice($children, $offset);

$products_1 = $container['rack_api']->menu_html($racks_1, true);
$container['helper']->save_static_block('products_1', $products_1);

$products_2 = $container['rack_api']->menu_html($racks_2);
$container['helper']->save_static_block('products_2', $products_2);
