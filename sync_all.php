<?php

/**
 * Script which syncs all the racks and categories.
 * Once sync is completed, categories are sorted by name.
 */
require_once __DIR__ . '/di.php';

$container['helper']->setIndexingManual(false);
// Racks Sync
$container['category_api']->touchRackMain();
$container['rack_api']->syncAll();

// Accessory Sync
$container['category_api']->touchCategoryMain();
$container['accessory_api']->syncAll();

// sort rack and accessory child categories
$container['helper']->sort_rack_accessory_categories();
$container['helper']->setIndexingManual(true);
//$container['helper']->reindex_all();
