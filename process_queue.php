<?php

/**
 * Script which processes a single rack and accessory queued for processing.
 */

require_once __DIR__ . '/di.php';

$container['rack_api']->processQueue();
$container['accessory_api']->processQueue();
