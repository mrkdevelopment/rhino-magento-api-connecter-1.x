![Rhino Racks Logo](http://www.rhinorack.com/app_themes/rr/images/rr-logo.png)

# [Guide] API Connector Module For Magento

## Features

The Rhino Racks API Connector Module offers the following features:

1. Ability to synchronize racks and accessories as Magento products.
2. Additional data received from the API such as featured description, instructions, media etc are stored as magento attributes. The products details page can use these attribute to provide more information.
3. Nested categories auto-creation for racks and accessories products. Racks are sub-categorized as `Manufacturer > Make > Year > Body`. Accessories are sub-categories as `Main Category > Sub Category`.
4. Rack category API endpoints for manufacturer, make, year and body useful to create a **Rack Selector** like the one found at [http://www.rhinorack.com/en-us/fit-my-vehicle/select-vehicle](http://www.rhinorack.com/en-us/fit-my-vehicle/select-vehicle)
5. A Rack Selector Widget. 

##Prerequisite

1. This plugin is entended to work on a Magento 1.8+ environement. Install a version of Magento before installing this extension.
2. You will need to run commands on your code to change file permissions. You need a Terminal access to your Magento access.

## Requirements

1. Magento 1.9.x
2. 2GB+ RAM recommended.
3. php >=5.4. Information on support versions for php can be found at [http://php.net/supported-versions.php](http://php.net/supported-versions.php)
4. In your php.ini please ensure that *recursion_limit* is set to a very high value. Recommended value is 1000; 
`[PCRE] 
pcre.recursion_limit = 1000`.
5. [Composer](https://getcomposer.org/) - a dependancy manager for PHP.


## Module Installation

1. Create a `RhinoRacks` directory in `app/code/local` using `mkdir app/code/local/RhinoRacks`. Create missing directories in the path, if needed.
2. Make sure the directory `media/catalog/product` exists or create the missing directories. This directory will store the product images fetched via the API.
2. Using a terminal, browse to your Magento install and to the RhinoRacks folter: `cd app/code/local/RhinoRacks`.
3. Clone this repository module using the following command: `git clone git@bitbucket.org:mrkdevelopment/rhino-magento-api-connecter-1.x.git APIConnector`.
4. Change directory `cd APIConnector`.
5. Run `composer install`.
6. Copy the RhinoRacks_APIConnector.xml file to `app/etc/modules` folder to register the module. You can use the following command `cp RhinoRacks_APIConnector.xml ../../../../etc/modules/`
7. If your magento is using cache (default), you need to empty it. Using a web browser, connect to the administration  panel and go to `System > Cache Management page`. Click on "Flush Magento Cache".
8. You need to refresh user's permissions. Visit `System > Permissions > Users`. Select your user account. and click the `Reset Filter` button. On the user details page, select the `User Role` tab. On that page make sure `Administrators` role is selected. Click `Reset Filter` button, then the `Save User` button. Logout of the admin and log back in.
9. To confirm the module installation, login to the admin section of your magento install and visit `System > Configuration > Advanced`. Click on the `Disable Modules Output` to deploy the section. If the plugin is active, you should see the module `RhinoRacks_APIConnector` marked as `Enable`


Note: If you see a 404 while loading or saving the configuration page, its most likely due to the permissions needing a refresh. Visit `System > Permissions > Users`. Select your user account. and click the `Reset Filter` button. On the user details page, select the `User Role` tab. On that page make sure `Administrators` role is selected. Click `Reset Filter` button, then the `Save User` button. Logout of the admin and log back in. Try this set again to setup the credentials.


## Module Configuration
1. To enter the Rhino Racks API credentials visit `System > Configuration` page and go to `Catalog > Rhino Rack configuration` section. 
2. Click on Account Details to deploy the section.
3. Enter the username/password details and click `Save Config`. 
4. Use a terminal to browse to your APIConnector folder `cd app/code/local/RhinoRacks/APIConnector`
5. run `php migration.php`. This will create a queue table, used in one of the workflow for syncing products (explained later in this document).


Module should be installed now and we can proceed next with sync products using command line.

## Syncing products from the Command Line.
 
Rhino Racks has thousands of Roof-Racks and Accessories. Syncing them all **initially** is a time-consuming process. Magento on the other hand uses database `transactions` to write data into the database. The combination of the two would requires us to follow a two-pronged approach.

1. Syncing up **all** the products initially (before the site is live preferrably so we can avoid database transaction locks).
2. Syncing only updated products periodically and synchronously, once the initially sync has been done, **using a database queue** to fetch data from the API and command line task with periodically polls the queue to create/update the products.

**Note:** We use the `GetRoofRacksForDate` and `GetAccessoriesByDate` API calls to fetch racks and accessories by date respectively. The last date reference information is stored in `app/code/local/RhinoRacks/APIConnector/etc/racks_datetime.txt` and `app/code/local/RhinoRacks/APIConnector/etc/accessories_datetime.txt` for racks and accessories respectively. If these files are not present, the sync process creates them as used `1970-01-01` as the start date.

### Initial Synchronization.

The initial synchronization, as noted above, is a **long running** process. It is designed to sync in all the products since `1970-01-01`. It also sorts the categories in the admin once all the products are synced.

The sync process creates 2 main categories i.e `Racks` and `Accessories`. The sub-categories within these main categories are **unordered alphabetically** and the ordering depends on their creation. The sync process runs a script to sort these categories. 

To run the initial synchronization using the following command

`php <absolute_path_to>/app/code/local/RhinoRacks/APIConnector/sync_all.php`

**Note**: Being a long running process, it is prone to termination due to factors like insufficient memory, API connectivity issues etc. It is **highly** recommended to run this process using a process monitoring software like [supervisor](http://supervisord.org/) or [monit](https://mmonit.com/monit/) 

**Question** : How do I know when the initial sync is over?

**Answer** : Check the contents of `app/code/local/RhinoRacks/APIConnector/etc/racks_datetime.txt` and `app/code/local/RhinoRacks/APIConnector/etc/accessories_datetime.txt` file for a very current timestamp. The sync usually completes in 1-2 days.
	
### Periodic Synchronization using queue.

Once the initial synchronization is over (and the website is live), we need to careful about causing issues with Magento's database transactions. For that very reason, we have created a workflow we use 2 command line scripts.

1. First script fetches 5 racks and 5 accessories for every single run and stores them to a database queue. **It is advisable to run it every 5 minutes via crontab**

	command: `php <absolute_path_to>/app/code/local/RhinoRacks/APIConnector/queue_racks_accessories.php`
2. The second script, polls the database queue and creates/updates 1 rack and 1 accessory each. **It is advisable to run it every 1 minutes via crontab**
	
	command: `php <absolute_path_to>/app/code/local/RhinoRacks/APIConnector/process_queue.php`
	
**Note:** Please ensure that the php script used to running the initial sync `sync_all.php` **isn't running** once periodic synchronization starts.


## Base Child Theme
We have created a child theme using **[rwd](http://devdocs.magento.com/guides/m1x/ce19-ee114/RWD_dev-guide.html#intro)** theme as the base. The base has the following features.

1. Product details page has additional data like featured description, instructions, image/video assets which are fetched from the API.
2. Javascript and style assets for the rack selector widget.

Find more information on how to use the theme at [https://bitbucket.org/mrkdevelopment/rhino-racks-magento-child-theme](https://bitbucket.org/mrkdevelopment/rhino-racks-magento-child-theme).

## Helper Utilities.

### Sorting Rack and Acccesories Sub-Categories.

If you ever need to sort rack and accessory sub-categories (used in magento standard template's top menu and category sidebar) use the following command

command: `php <absolute_path_to>/app/code/local/RhinoRacks/APIConnector/sort_categories.php`


### Cleaning up the periodic sync queue.

Once a queue item is processed, its marked as completed in the queue. Its not auto-deleted. They occupy important disc space and could potentially slow down the queue table query in the long run. One can run a daily/weekly cleanup script via the crontab to cleanup.

command: `php <absolute_path_to>/app/code/local/RhinoRacks/APIConnector/clear_queue.php`
