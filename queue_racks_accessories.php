<?php

/**
 * Script which reads 5 racks and accessories each from the API and adds it to the queue.
 */
require_once __DIR__ . '/di.php';

$container['rack_api']->addToQueue();
$container['accessory_api']->addToQueue();
