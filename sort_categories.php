<?php

/**
 * Script which sorts the rack and accessory sub categories by name.
 * Reindexes the indexes.
 */

require_once __DIR__ . '/di.php';

// Sort Racks and Accessory child categories
$container['helper']->sort_rack_accessory_categories();
$container['helper']->reindex_all();
