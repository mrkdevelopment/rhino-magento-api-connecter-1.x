<?php
/**
 * Controller class for the Rack Selector API Endpoints.
 */
class RhinoRacks_APIConnector_RackCategoriesController extends Mage_Core_Controller_Front_Action
{

    /**
     * Manufacturer API end-point.
     *
     * @return string json response
     */
    public function manufacturersAction()
    {
        $container = $this->getContainer();
        $container['helper']->json(array_values($container['category_api']->getManufacturers()));
    }

    /**
     * Model API end-point.
     *
     * @return string json response
     */
    public function modelsAction()
    {
        $container = $this->getContainer();
        $container['helper']->json(array_values($container['category_api']->getModels($_GET['manufacturer_id'])));
    }

    /**
     * Year API end-point.
     *
     * @return string json response
     */
    public function vehicleAction()
    {
        $container = $this->getContainer();
        $container['helper']->json(array_values($container['category_api']->getVehicles($_GET['year_id'])));
    }

    /**
     * Years json.
     */
    public function yearAction()
    {
        $container = $this->getContainer();
        $years = array_values($container['category_api']->getYears($_GET['model_id']));
        if (is_array($years)) {
            usort($years, function ($first, $second) {
                if ($first['name'] == $second['name']) {
                    return 0;
                }

                return ($first['name'] < $second['name']) ? -1 : 1;
            });
        }

        $container['helper']->json(array_reverse($years));
    }

    /**
     * Get Dependancy Injection Container.
     *
     * @return \Pimple\Container container
     */
    private function getContainer()
    {
        require_once __DIR__ . '/../vendor/autoload.php';
        $di = new RhinoRacks_DI();

        return $di->container;
    }
}
