<?php


// Load magento
require_once  __DIR__ .'/../../../../Mage.php';
Mage::app();

/**
 * Wrapper script to initialize container, load Magento and a debug helper.
 */

require_once __DIR__ . '/vendor/autoload.php';

$di = new RhinoRacks_DI();
$container = $di->container;
