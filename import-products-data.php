<?php

/**
 * Reads CSV file and import data into products.
 */

require_once __DIR__ . '/di.php';

$file = __DIR__ . '/' . $argv[1];
$container['helper']->import($file);
