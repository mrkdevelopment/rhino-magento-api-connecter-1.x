<div class="std">
<form id='rack-selector-form'>
    <ul class='rack-selector-container'>
        <li><label for="">Make</label>&nbsp;<select name="" id="make"></select></li>
        <li><label for="">Model</label>&nbsp;<select name="" id="model"></select></li>
        <li><label for="">Vehicle</label>&nbsp;<select name="" id="vehicle"></select></li>
        <li><button type='submit' class='button'>Find Racks</button></li>
    </ul>
</form>


<?php
    $skinBase = Mage::getDesign()->getSkinUrl('');
?>


<link rel="stylesheet" href="<?php echo $skinBase ?>css/rack-selector.css">

<script src='<?php echo $skinBase ?>js/rack-selector.js'></script>
</div>
