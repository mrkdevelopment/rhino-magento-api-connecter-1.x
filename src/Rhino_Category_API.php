<?php
/**
 * Class having rack category api data fetcher, magento category read/write
 * operations specific to rack and accessory category/sub-categories.
 */
class Rhino_Category_API
{
    /**
     * Pimple Container
     *
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * Constructor
     *
     * @param \Pimple\Container $container Container.
     */

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Get all manufacturer categories for racks
     *
     * @return array Manufacturer categories.
     */
    public function getManufacturers()
    {
        $this->touchRackMain();

        return $this->getChildrenCategories($this->container['racks_category']);
    }

    /**
     * Helper function to get the children categories.
     *
     * @param Category $parent Parent Category
     *
     * @return array Child categories
     */
    public function getChildrenCategories($parent)
    {
        $this->container['cache']->setCache('children_categories');

        $parent_id = $parent->getId();

        if ($this->container['cache']->isCached($parent_id)) {
            return $this->container['cache']->retrieve($parent_id);
        } else {
            $children = array();
            foreach ($parent->getChildrenCategories() as $category) {
                $children[$category->getName()] = array(
                                    'id'   => (int)$category->getId(),
                                    'name' => $category->getName(),
                                    'url'  => $category->getUrl(),
                                    );
            }

            ksort($children);
            $this->container['cache']->store($parent_id, $children, 21600); //6 hours

            return $children;
        }
    }

    /**
     * Get rack models category for a given manufacturer id
     *
     * @param int $id manufacturer id
     *
     * @return array model categories
     */
    public function getModels($id)
    {
        return $this->getChildrenById($id);
    }

    /**
     * Get rack years category for a given model id
     *
     * @param int $id model id
     *
     * @return array year categories
     */
    public function getVehicles($id)
    {
        return $this->getChildrenById($id);
    }

    /**
     * Get rack year category for a given year model category id
     *
     * @param int $id year category id
     *
     * @return array year categories
     */
    public function getYears($id)
    {
        return $this->getChildrenById($id);
    }

    /**
     * Get children categories given a parent id.
     *
     * @param int $id parent id
     *
     * @return array child categories
     */
    public function getChildrenById($id)
    {
        $parent = Mage::getModel('catalog/category')
                ->setStoreId($this->container['store_id']);

        $parent->load($id);

        return $this->getChildrenCategories($parent);
    }

    /**
     * Touch the main rack category
     *
     */
    public function touchRackMain()
    {
        $this->container['racks_category'] = $this->container['category_api']->firstOrCreateCategory('Racks', $this->container['main_category'], true);
    }

    /**
     * Touch category main.
     *
     */
    public function touchCategoryMain()
    {
        $this->container['accessories_category'] = $this->container['category_api']->firstOrCreateCategory('Accessories', $this->container['main_category'], true);
    }

    /**
     *  Helper function to check if category for given parent exists,
     * if not create one
     *
     * @param string   $name    Category name
     * @param Category $parent  Parent Category
     * @param boolean  $isMain  Indicate if its main Rack/Accessory category.
     *
     * @return Category Category
     */
    public function firstOrCreateCategory($name, $parent, $isMain = false)
    {
        // var_dump($parent);
        foreach ($parent->getChildrenCategories() as $category) {
            if ($category->getName() == $name) {
                return $category;
            }
        }

        return $this->createNestedCategory($name, $parent, $isMain);
    }

    /**
     * Creates a category for a give parent and ancestor path ids
     *
     * @param string   $name    Category name
     * @param Category $parent  parent category
     * @param boolean  $isMain  Indicate if its main Rack/Accessory category.
     *
     * @return Category Category
     */
    public function createNestedCategory($name, $parent, $isMain = false)
    {
        $category = Mage::getModel('catalog/category')
                ->setStoreId($this->container['store_id']);

        $category->setName($name);
        $category->setUrlKey(str_replace(' ', '-', strtolower($name)));
        $category->setIsActive(1);
        $category->setIsAnchor(1);

        $category->setAttributeSetId($category->getDefaultAttributeSetId());
        $category->setParentId($parent->getId());
        $category->setIncludeInMenu(1);

        $category->setAvailableSortBy(array('price'));
        $category->setDefaultSortBy('price');

        $category->setPath($parent->getPath());

        $validate = $category->validate();
        if ($validate !== true) {
            // dd($validate);
        }
        $category->save();

        return $category;
    }
}
