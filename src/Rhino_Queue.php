<?php
/**
 * Queue helper class to deal with periodic sync items.
 */
class Rhino_Queue
{

    /**
     * Pimple Container
     *
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * Queue table name
     *
     * @var string
     */
    protected $table_name;

    /**
     * Constructor
     *
     * @param \Pimple\Container $container Container.
     */
    public function __construct($container)
    {
        $this->container  = $container;
        $this->table_name = $container['db_prefix'] . 'rhino_racks_magento_queue';
    }

    /**
     * Store rack/accessory type in a queue.
     *
     * @param string $type rack/accessory
     * @param array  $data Data from the API
     *
     * @return void
     */
    public function store($type, $data)
    {
        // using PDO quote for escaping json_encode special characters
        $data  = $this->container['db_writer']->quote(json_encode($data));

        $query = sprintf("INSERT INTO `%s` (`type`, `data`, `status`) VALUES ('%s', %s, '%s');", $this->table_name, $type,  $data, 'queued');

        $this->container['db_writer']->query($query);
    }

    /**
     * Get a queue item for processing.
     *
     * @param string $type rack/accessory
     *
     * @return array query result
     */
    public function get($type, $limit = 1)
    {
        $query  = sprintf('SELECT * FROM `%s` WHERE type="%s" and status="queued" LIMIT %s', $this->table_name, $type, $limit);

        return $this->container['db_reader']->fetchAll($query);
    }

    /**
     * Mark a queue item as complete.
     *
     * @param int $id ID
     *
     * @return void
     */
    public function markComplete($id)
    {
        $this->setStatus($id, 'complete');
    }

    /**
     * Sets a status to queue item
     *
     * @param int    $id     ID
     * @param string $status status
     */
    public function setStatus($id, $status)
    {
        $query = sprintf('UPDATE `%s` SET status="%s" WHERE id=%s LIMIT 1;', $this->table_name, $status, $id);

        $this->container['db_writer']->query($query);
    }

    /**
     * Clean up completed queue.
     *
     * @return void
     */
    public function clear()
    {
        $query = sprintf('DELETE FROM `%s` WHERE status="%s";', $this->table_name, 'complete');

        $this->container['db_writer']->query($query);
    }

    /**
     * Truncate queue if empty.
     * Mitigates issues where queue increments reach thresholds.
     *
     * @return void
     */
    public function truncateIfEmpty()
    {
        $query  = sprintf('SELECT count(*) as total FROM `%s`', $this->table_name);

        $response = $this->container['db_reader']->fetchAll($query);

        if ($response[0]['total'] === '0') {
            $this->container['db_writer']->query(sprintf('truncate `%s`', $this->table_name));
        }
    }
}
