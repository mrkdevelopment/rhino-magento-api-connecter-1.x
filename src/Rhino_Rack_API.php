<?php


/**
 * Rhino Rack related code.
 */
class Rhino_Rack_API
{

    /**
     * Pimple Container
     *
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * Constructor
     *
     * @param \Pimple\Container $container Container.
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Fetch racks from API and add to queue.
     */
    public function addToQueue()
    {
        $params                   = array();
        $params['dateFromString'] = $this->container['helper']->getDateTime('racks_datetime.txt');

        list($racks, $params) = $this->container['rack_api']->getRacksFromAPI($params);

        foreach ($racks as $rack) {
            $rack['categories'] = $this->container['rack_api']->getRackCategories($rack['id']);

            if ($rack['feature_image']) {
                $rack['image']    = $this->container['helper']->downloadImage($rack['feature_image']);
            }

            $this->container['queue']->store('rack', $rack);

            $this->container['helper']->setDateTime('racks_datetime.txt', $rack['last_updated']);
        }
    }

    /**
     * Process a rack queue item and save product into magento.
     *
     */
    public function processQueue()
    {
        $items = $this->container['queue']->get('rack');
        $this->container['category_api']->touchRackMain();

        foreach ($items as $item) {
            $rack                           = json_decode($item['data'], true);
            $categories                     = $rack['categories'];

            $categoryIds                    = $this->container['rack_api']->touchCategories($categories);
            $product                        = $this->container['helper']->syncProduct($rack, $categoryIds);

            $this->container['queue']->markComplete($item['id']);
        }
    }

    /**
     * Sync All Racks using start datetime stamp from the racks_datetime.txt file.
     *
     * @return void
     */
    public function syncAll()
    {
        $date    = $this->container['helper']->getDateTime('racks_datetime.txt');

        $params                   = array();
        $params['dateFromString'] = $date;

        $racks  = true;

        while ($racks) {
            list($racks, $params) = $this->container['rack_api']->getRacksFromAPI($params);

            foreach ($racks as $rack) {
                $categories                 = $this->container['rack_api']->getRackCategories($rack['id']);
                $categoryIds                = $this->container['rack_api']->touchCategories($categories);

                $product = $this->container['helper']->syncProduct($rack, $categoryIds);
                $this->container['helper']->setDateTime('racks_datetime.txt', $rack['last_updated']);
            }
        }
    }

    /**
     * Get Racks from the API based on params and start date time.
     *
     * @param array $params API parameters.
     *
     * @return array array containing racks and params
     */
    public function getRacksFromAPI($params)
    {
        $this->container['helper']->touchBaseAttributes();

        $params['status']         = 1;

        if (!isset($params['pageIndex'])) {
            $params['pageIndex'] = 1;
        }

        if (!isset($params['pageSize'])) {
            $params['pageSize'] = 5;
        }

        $sync = new \RhinoRacks\Sync;

        $racks = $sync->racks($this->container['auth'], $params);

        $params['pageIndex'] = $params['pageIndex'] + 1;

        return array($racks, $params);
    }

    /**
     * Gets the categories for a rack from the API.
     *
     * @param int $id ID
     *
     * @return array categories.
     */
    public function getRackCategories($id)
    {
        $vehicles = new \RhinoRacks\API\Vehicles($this->container['auth']);

        $vehicles->GetVehiclesByRoofRack($id);

        $result = json_decode(json_encode($vehicles->result()));

        if (!isset($result->Vehicle)) {
            return array();
        }

        // normalize array.
        if (is_array($result->Vehicle)) {
            $items = $result->Vehicle;
        } else {
            $items = array($result->Vehicle);
        }

        $response = array();

        foreach ($items as $vehicle) {
            if (!is_string($vehicle->Manufacturer) || !is_string($vehicle->Model)) {
                continue;
            }

            // compute data
            $make                     = (string) $vehicle->Manufacturer;
            $model                    = (string) $vehicle->Model;
            $excerpt                     = (string) $vehicle->VehicleExcerpt;

            list($startYear, $others) = explode('-', (string) $vehicle->StartDate, 2);
            if (!is_string($vehicle->EndDate)) {
                        $years   = array((int)$startYear);
                        $endYear = date('Y', time());
            } else {
                        list($endYear, $others) = explode('-', (string) $vehicle->EndDate, 2);
                        $years                  = array();
            }

            foreach (range($startYear, $endYear) as $year) {
                if (!in_array($year, $years)) {
                    $years[] = $year;
                }
            }
            
            $response[] = array('make' => $make, 'model' => $model, 'excerpt' => $excerpt, 'years' => $years);
        }

        return $response;
    }

    /**
     * Touch the nested categories for the rack.
     *
     * @param array $categories Categories
     *
     * @return array Category IDs.
     */
    public function touchCategories($categories)
    {
        $c   = $this->container;
        $ids = array();

        foreach ($categories as $category) {
            // manufacturer
            $make    = $c['category_api']->firstOrCreateCategory($category['make'], $c['racks_category']);
            $ids[]   = $make->getId();

            // Model
            $model   = $c['category_api']->firstOrCreateCategory($category['model'], $make);
            $ids[]   = $model->getId();

            $yearCategories = array();
        
            foreach ($category['years'] as $year) {
                $yearCategory = $c['category_api']->firstOrCreateCategory($year, $model);
                $ids[]                 = $yearCategory->getId();

                //excerpt
                $excerpt    = $c['category_api']->firstOrCreateCategory($category['excerpt'], $yearCategory);
                $ids[]   = $excerpt->getId();
            }
        }

        unset($categories);

        return $ids;
    }

    /**
     * Generates the menu for the racks
     *
     * @param array $racks Rack categories/subcategories
     *
     * @return string List HTML
     */
    public function menu_html($racks, $addMain = false)
    {
        $menu   = array("<ul>");

        if ($addMain) {
            $main = $this->container['racks_category'];

            $menu[] = sprintf("<li><strong><a href='%s'>RACKS - MANUFACTURER</a></strong></li>", $main->getUrl());
        }

        foreach ($racks as $rack) {
            $menu[] = sprintf("<li><a href='%s'>%s</a></li>", $rack['url'], $rack['name']);
        }

        $menu[] = "</ul>";

        return implode("", $menu);
    }
}
