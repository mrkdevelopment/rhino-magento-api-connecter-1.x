<?php

/**
 * Helper class.
 */
class Rhino_Helper
{
    /**
     * Pimple Container
     *
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * Allowed product attributes
     *
     * @var array
     */
    protected $attributes ;

    protected $weightAttributeId;

    /**
     * Constructor
     *
     * @param \Pimple\Container $container Container.
     */
    public function __construct($container)
    {
        $this->container  = $container;
        $this->attributes = array();
    }


    /**
     * get websafe image url (one will lower payload.)
     */
    public function get_websafe_image_url($url)
    {
        $pathinfo = pathinfo($url);
        return sprintf("%s/%s_lrg.%s", $pathinfo['dirname'], $pathinfo['filename'], $pathinfo['extension']);
    }

    /**
     * Download Image
     *
     * @param string $url Image URL
     *
     * @return string absolute local path.
     */
    public function downloadImage($url)
    {
        $url = $this->get_websafe_image_url($url);

        $relativePath = 'media/catalog/product/' . pathinfo($url, PATHINFO_BASENAME);
        $localPath    = Mage::getBaseDir() . '/' . $relativePath;
        if (file_exists($localPath)) {
            return $localPath;
        }

        set_time_limit(0);
        $fp = fopen($localPath, 'w+');//This is the file where we save the    information
        $ch = curl_init(str_replace(" ", "%20", $url));//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);

        fclose($fp);

        return $localPath;
    }

    /**
     * Writes the datetime timestamp to the rack/category txt file.
     * Used for the get rack/accessory by date API calls.
     *
     * @param string $name     File name
     * @param string $datetime date time
     */
    public function setDateTime($name, $datetime)
    {
        $path = $this->container['base_path'] . '/etc/' . $name;
        file_put_contents($path, $datetime);
    }

    /**
     * Reads the datetime timestamp for the given rack/category file.
     *
     * @param string $name filename
     *
     * @return string timestamp.
     */
    public function getDateTime($name)
    {
        $path = $this->container['base_path'] . '/etc/' . $name;

        if (file_exists($path)) {
            return file_get_contents($path);
        }

        $datetime = '1970-01-01';
        $this->setDateTime($name, $datetime);

        return $datetime;
    }

    /**
     * Add an attribute to magento product
     *
     * @param string $name  name of the product
     * @param string $input type of the attribute (text/textarea etc)
     */
    public function addAttribute($name, $input = 'text')
    {
        $slug = $this->attributeSlug($name);

        $model=Mage::getModel('eav/entity_setup', 'core_setup');

        $data=array(
            'type'           => 'text',
            'input'          => $input,
            'label'          => $name,
            'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'is_required'    => '0',
            'is_comparable'  => '0',
            'is_searchable'  => '0',
            'is_unique'      => '1',
            'is_configurable'=> '1',
            'use_defined'    => '1',
            );

        $response = $model->addAttribute('catalog_product', $slug, $data);
    }

    /**
     * Get the magento product attribute slug in desired format
     *
     * @param string $name Attribute name
     *
     * @return string attribute slug.
     */
    public function attributeSlug($name)
    {
        return str_replace(' ', '_', strtolower($name));
    }

    /**
     * Check to see if an attribute is allowed to be associated with a product
     *
     * @param string $key   Attribute name
     * @param string $value Attribute value
     *
     * @return boolean
     */
    public function allowedAttribute($key, $value)
    {
        $slug = $this->attributeSlug($key);

        if (isset($this->attributes[$slug])) {
            return true;
        }

        if (preg_match('/^DK\d(\d)*$/i', $key) === 1) {
            return false;
        }

        $this->addAttribute($key);

        $this->attributes[$slug] = true;

        return true;
    }

    /**
     * Create attributes universally present in a rack/accessory
     *
     * @return void
     */
    public function touchBaseAttributes()
    {
        $this->addAttribute('Feature Description', 'textarea');
        $this->addAttribute('Media', 'textarea');
        $this->addAttribute('Asset Links', 'textarea');
        $this->addAttribute('components', 'textarea');
        $this->addAttribute('load_ratings', 'textarea');
        $this->addAttribute('rr_id', 'text');
        $this->addAttribute('last_updated', 'text');
        $this->addAttribute('specifications', 'text');
    }

    /**
     * Create/Update a magento product.
     *
     * @param array $data product information.
     *
     * @return Product Magento product
     */
    public function syncProduct($data, $categoryIds)
    {
        echo sprintf("Saving %s with SKU %s\n", $data['name'], $data['stock_code']);
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $mediaApi = Mage::getModel("catalog/product_attribute_media_api");

         $images     = array();
        foreach ($data['media']['images'] as $img) {
            $images[] = $img['url'];
        }

        if ($data['feature_image']) {
            if (!in_array($data['feature_image'], $images)) {
                $images[] = $data['feature_image'];
            }
        }


  
        // remove duplicates.
        $images = array_unique($images);

        foreach ($images as $image) {
            $this->downloadImage($image);
        }



        $product = Mage::getModel('catalog/product');
        if (!$product->getIdBySku($data['stock_code'])) {
            $product->setCreatedAt(strtotime('now')) //product creation time
            ->setWeight(1)
            ->setStockData(array(
                          'use_config_manage_stock'  => 0, //'Use config settings' checkbox
                           'manage_stock'            => 0, //manage stock
                           ));
        } else {
            $product->load($product->getIdBySku($data['stock_code']));
            $gallery_images = $product->getMediaGalleryImages();
            foreach ($gallery_images as $gallery_image) {
                $mediaApi->remove($product->getId(), $gallery_image->getFile());
            }
        }

        //media gallery initialization
        $product->setMediaGallery(array('images'=> array (), 'values'=>array ()));

        // Image gallery - API returns the featured image separately
        // and an array of additional images which again includes the featured image as the last one.
        $imageCount = count($images);
        for ($i = ($imageCount - 1); $i >= 0; $i--) {
            $path = $this->downloadImage($images[$i]);

            if (file_exists($path)) {
                try {
                    $product->addImageToMediaGallery($path, array('image', 'thumbnail', 'small_image'), true, false);
                } catch (\Exception $e) {
                    // Something went wrong with Switch API returning incorrect image or error in downloading image. Ignore.
                    echo sprintf("[Error] Error processing image %s\n", $images[$i]);
                }
            }
        }


        try {
            $product
            ->setWebsiteIds(array($this->container['website_id'])) //website ID the product is assigned to, as an array
            ->setSku($data['stock_code'])
            ->setAttributeSetId($product->getDefaultAttributeSetId()) //ID of a attribute set named 'default'
            ->setTypeId('simple') //product type
            ->setUpdatedAt(strtotime('now')) //product update time
            ->setName($data['name']) //product name
            ->setStatus(1) //product status (1 - enabled, 2 - disabled)
            ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
            ->setDescription($data['description'] ? $data['description'] : $data['list_description'])
            ->setShortDescription($data['list_description']);

            if (!empty($categoryIds)) {
                $product->setCategoryIds($categoryIds); //assign product to categories
            }

            if ($this->container['use_price']) {
                $product->setPrice((float) $data['price']);
            }

            $product->save();

            $product->addAttributeUpdate('rr_id', $data['id'], $this->container['store_id']);
            if (isset($data['last_updated'])) {
                $product->addAttributeUpdate('last_updated', $data['last_updated'], $this->container['store_id']);
            }
            // specifications.
            if (!isset($data['specifications'])) {
                $data['specifications'] = array();
            }
             $product->addAttributeUpdate('specifications', json_encode($data['specifications']), $this->container['store_id']);

            if (isset($data['components'])) {
                $product->addAttributeUpdate('components', json_encode($data['components']), $this->container['store_id']);
            }
            if (isset($data['load_ratings'])) {
                $product->addAttributeUpdate('load_ratings', json_encode($data['load_ratings']), $this->container['store_id']);
            }


            // Touch attributes if not present
            if (isset($data['feature_description'])) {
                $product->addAttributeUpdate('feature_description', $data['feature_description'], $this->container['store_id']);
            }
            if (isset($data['media'])) {
                $product->addAttributeUpdate('media', json_encode($data['media']), $this->container['store_id']);
            }
            if (isset($data['links'])) {
                $product->addAttributeUpdate('asset_links', json_encode($data['links']), $this->container['store_id']);
            }
        } catch (\Exception $e) {
            Mage::log($e->getMessage());
        }

            return $product;
    }

    /**
     * Helper function to write out json response
     *
     * @param array $data Data to be output
     *
     * @return string JSON response.
     */
    public function json($data)
    {
        echo header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }

    /**
     * Helper function to save to a magento static block
     *
     * @param string $identifier static block identifier
     * @param string $content    data to be written
     *
     * @return boolean
     */
    public function save_static_block($identifier, $content)
    {
        $cmsBlock = Mage::getModel('cms/block')
        ->load($identifier);

        if ($cmsBlock->isObjectNew()) {
            $cmsBlock->setTitle($identifier)
            ->setIdentifier($identifier);
        }

        $cmsBlock->setTitle($identifier)
        ->setContent($content)
        ->setStores($this->container['store_id'])
        ->setIsActive(true);

        $cmsBlock->save();
    }

    /**
     * Helper to reindex all.
     */
    public function reindex_all()
    {
        $indexCollection = Mage::getModel('index/process')->getCollection();
        foreach ($indexCollection as $index) {
            $index->reindexAll();
        }
    }

    /**
     * Helper function to recursively sort the subcategories in racks/accessories
     * alphabetically in ascending order
     *
     * @return void
     */
    public function sort_rack_accessory_categories()
    {
        // Racks
        $this->container['category_api']->touchRackMain();
        $this->container['helper']->sort_categories_by_name($this->container['racks_category']->getId());

        // Accessories
        $this->container['category_api']->touchCategoryMain();
        $this->container['helper']->sort_categories_by_name($this->container['accessories_category']->getId());
    }

    /**
     * Sort the child categories by name
     *
     * @param int $parentId parent id
     *
     * @return void
     */
    public function sort_categories_by_name($parentId)
    {
        //The position field is in the main category table
        //add prefix if you have one
        $table = sprintf('%scatalog_category_entity', $this->container['db_prefix']);

        //get the children for a specific category id
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter('parent_id', $parentId);
        $categories = array();
        foreach ($collection as $category) {
            //in case there are 2 categories with the same name we shouldn't skip them
            //but there shouldn't be
            $categories[$category->getName().'_'.$category->getId()] = $category->getId();
        }

        //sort categories by name
        ksort($categories);

        //set their position
        $position = 1;

        foreach ($categories as $name => $id) {
            $q = "UPDATE {$table} SET `position` = {$position} where `entity_id` = {$id}";
            $this->container['db_writer']->query($q);
            $position++;
            //sort the current category children by calling the same function recursively
            $this->sort_categories_by_name($id);
        }
    }



       /**
     * Import CSV file data into products.
     */
    public function import($file)
    {
        $row = 0;
        if (($handle = fopen($file, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $row++;
                if ($row == 1) {
                    continue; //skip headers
                }
                $this->updateProductWithCsvData($data);
            }
            fclose($handle);
        }
    }


    /**
     * Update product
     */
    public function updateProductWithCsvData($data)
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $product = Mage::getModel('catalog/product');

        if ($id = $product->getIdBySku($data[0])) {
            $exists = $this->weightExists($id, $this->getWeightAttributeId());
            if (!$exists) {
                $this->insertWeight($id, $data[1], $this->getWeightAttributeId(), 4);
            }
            try {
                //update weight
    
                /**
                 * Get the resource model
                 */
                $resource = Mage::getSingleton('core/resource');
    
                /**
                 * Retrieve the write connection
                 */
                $writeConnection = $resource->getConnection('core_write');
               
                /**
                 * Retrieve our table name
                 */
                $table = $resource->getTableName('catalog_product_entity_decimal');
                $sql = sprintf('UPDATE %s  SET
                            value = %s
                        WHERE
                            entity_id = %s and attribute_id=%s;', $table, (float) $data[1], $id, $this->getWeightAttributeId());
                $writeConnection->query($sql);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }


    /**
     * Insert Weight.
     */
    public function insertWeight($entityId, $weight, $attributeId, $entityTypeId)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $table = $resource->getTableName('catalog_product_entity_decimal');
        $writeConnection = $resource->getConnection('core_write');

        $sql = sprintf('INSERT INTO %s (entity_type_id, attribute_id, store_id, entity_id, value) VALUEs 
(%s, %s, 0, %s, %s);', $table, $entityTypeId, $attributeId, $entityId, $weight);
        try {
            $writeConnection->query($sql);
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * Check if weight exists.
     */
    public function weightExists($entityId, $attributeId)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $table = $resource->getTableName('catalog_product_entity_decimal');
        $sql = sprintf("SELECT count(*) FROM %s where entity_id = %s and attribute_id=%s;", $table, $entityId, $attributeId);
        return ((int) $readConnection->fetchOne($sql));
    }

    /**
     * get weight attribute id.
     */
    public function getWeightAttributeId()
    {
        if (!$this->weightAttributeId) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $attributeTable = $resource->getTableName('eav_attribute');
            $attributeIdSql = sprintf("SELECT attribute_id FROM %s where attribute_code = 'weight';", $attributeTable);
            $this->weightAttributeId = $readConnection->fetchOne($attributeIdSql);
        }

        return $this->weightAttributeId;
    }


    public function setIndexingManual($value = true)
    {
    // set the magento indexing status to auto
        $pCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();

        if ($value) { // TRUE
            $mode = Mage_Index_Model_Process::MODE_REAL_TIME;
        } else {
            $mode = Mage_Index_Model_Process::MODE_MANUAL;
        }

        foreach ($pCollection as $process) {
            $process->setMode($mode)->save();
        }
    }
}
