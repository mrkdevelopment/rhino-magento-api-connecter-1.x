<?php
/**
 * Rhino Rack Accessory related code for bulk synchronization,
 * synchronization via queue and few helper classes
 */
class Rhino_Accessory_API
{

    /**
     * Pimple Container
     *
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * Constructor
     *
     * @param \Pimple\Container $container Container.
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Add accessory API data to queue and set for processing.
     */
    public function addToQueue()
    {
        $params               = array();
        $params['dateString'] = $this->container['helper']->getDateTime('accessories_datetime.txt');

        list($accessories, $params) = $this->getAccessoriesFromAPI($params);

        foreach ($accessories as $accessory) {
            if ($accessory['feature_image']) {
                $accessory['image']    = $this->container['helper']->downloadImage($accessory['feature_image']);
            }

            $this->container['queue']->store('accessory', $accessory);

            $this->container['helper']->setDateTime('accessories_datetime.txt', $accessory['last_updated']);
        }
    }

    /**
     * Process an accessory queue item
     */
    public function processQueue()
    {
        $items = $this->container['queue']->get('accessory');
        $this->container['category_api']->touchCategoryMain();

        foreach ($items as $item) {
            $accessory                  = json_decode($item['data'], true);
            $categoryIds                = $this->container['accessory_api']->touchCategories($accessory['categories']);
            $product                    = $this->container['helper']->syncProduct($accessory, $categoryIds);
            $this->container['queue']->markComplete($item['id']);
        }
    }

    /**
     * Sync All accessories until non are left.
     */
    public function syncAll()
    {
        $date = $this->container['helper']->getDateTime('accessories_datetime.txt');

        $params                   = array();
        $params['dateString']     = $date;

        $accessories          = true;

        $sync = new \RhinoRacks\Sync;

        while ($accessories) {
            list($accessories, $params) = $this->getAccessoriesFromAPI($params);

            foreach ($accessories as $accessory) {
                $categoryIds = array();
                //check if category is in ignore list.
                if (!$this->ignoreCategory($accessory['categories'])) {
                    foreach ($accessory['categories'] as $category) {
                        $categoryIds[]                = $this->container['accessory_api']->touchCategories($category);
                        // dump($categoryIds);
                    }
                } else {
                        echo sprintf("\nIgnored category - %s, %s\n", $accessory['categories'][0]['main'], $accessory['categories'][0]['sub']);
                }
            

                $product = $this->container['helper']->syncProduct($accessory, $categoryIds);
                $this->container['helper']->setDateTime('accessories_datetime.txt', $accessory['last_updated']);
            }
        }
    }


    /**
     * Category ignores.
     */
    public function ignoreCategory($categories)
    {
        // return false;
        $ignores = array();

        if (isset($categories['main'])) {
            $categories = array($categories);
        }

        $ignores[] = array(
        'main' => 'Roof Rack Parts',
        'sub' => 'Complete Roof Racks',
        );

        foreach ($categories as $category) {
            // ignore everything from Roof Racks.
            if ($category['main'] == 'Roof Racks') {
                return true;
            }

            // use the ignores array to check if its ignored.
            foreach ($ignores as $ignore) {
                if (($category['main'] ==  $ignore['main']) && ($category['sub'] == $ignore['sub'])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get Accessores from API.
     *
     * @param array $params Input data
     *
     * @return array array of accessories and parameters.
     */
    public function getAccessoriesFromAPI($params)
    {
        $this->container['helper']->touchBaseAttributes();

        $params['status']         = 1;

        if (!isset($params['pageIndex'])) {
            $params['pageIndex'] = 1;
        }

        if (!isset($params['pageSize'])) {
            $params['pageSize'] = 5;
        }

        $sync = new \RhinoRacks\Sync;

        $accessories = $sync->accessories($this->container['auth'], $params);

        $params['pageIndex'] = $params['pageIndex'] + 1;

        return array($accessories, $params);
    }

    /**
     * Touch the nested main and subcategory categories for the accessory.
     *
     * @param array $categories Category information.
     *
     * @return array category ids.
     */
    public function touchCategories($categories)
    {
        $c   = $this->container;
        $ids = array();

        if (isset($categories['main'])) {
            $categories = array($categories);
        }

        $main    = $c['category_api']->firstOrCreateCategory($categories[0]['main'], $c['accessories_category']);
        $ids[]   = $main->getId();

        $sub     = $c['category_api']->firstOrCreateCategory($categories[0]['sub'], $main);
        $ids[]   = $sub->getId();

        unset($categories);

        return $ids;
    }

    /**
     * Helper function to create html for a nested accessories
     *
     * @param array   $accessories accessories
     * @param integer $level       category level
     *
     * @return [type] [description]
     */
    public function menu_html($accessories, $level = 1)
    {
        if ($level == 1) {
            $menu   = array("<ul>");
        } else {
            $menu = array();
        }

        foreach ($accessories as $accessory) {
            $menu[] = '<li>';
            if ($level == 1) {
                $menu[] = "<strong>";
            }
            $menu[] = sprintf("<a href='%s'>%s</a>", $accessory['url'], $accessory['name']);

            if ($level == 1) {
                $menu[] = "</strong>";
            }

            $children = $this->container['category_api']->getChildrenById($accessory['id']);

            if ($children) {
                $menu[] = $this->menu_html($children, 2);
            }

            $menu[] = '</li>';
        }

        if ($level == 1) {
            $menu[] = "</ul>";
        }

            return implode("", $menu);
    }
}
