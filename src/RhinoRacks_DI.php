<?php

use Pimple\Container;

/**
 * Class used to define the dependancy injections.
 */
class RhinoRacks_DI
{

    /**
     * Pimple Container
     *
     * @var \Pimple\Container
     */
    public $container;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Initialize the dependencies.
     *
     * @return void
     */
    public function init()
    {
        $this->container = new Container();

        $this->container['base_path'] = __DIR__ . '/..';

        $this->container['main_category_id'] = Mage::app()->getWebsite(true)->getDefaultStore()->getRootCategoryId();
        $this->container['store_id']         = Mage::app()->getStore()->getStoreId();
        $this->container['website_id']       = Mage::app()->getStore()->getWebsiteId();

        // Get Username password values from catalog config.
        $username          = Mage::getStoreConfig('apiconnector_options/section_one/custom_field_one', $this->container['store_id']);
        $password          = Mage::getStoreConfig('apiconnector_options/section_one/custom_field_two', $this->container['store_id']);

        $this->container['auth'] = function ($c) use ($username, $password) {
            return array(
                    'username' => $username,
                    'password' => $password,
              );
        };

        $use_price  = Mage::getStoreConfig('apiconnector_options/section_one/sync_price_option', $this->container['store_id']);

        $this->container['use_price'] = function ($c) use ($use_price) {
            return ($use_price === "0") ? false : true;
        };

        $this->container['main_category'] = function ($c) {
            $parent = Mage::getModel('catalog/category')
                ->setStoreId($c['store_id'])
                ->load($c['main_category_id']);

            return $parent;
        };

        $this->container['category_api'] = $this->container->factory(function ($c) {
            return new Rhino_Category_API($c);
        });

        $this->container['rack_api'] = $this->container->factory(function ($c) {
            return new Rhino_Rack_API($c);
        });

        $this->container['accessory_api'] = $this->container->factory(function ($c) {
            return new Rhino_Accessory_API($c);
        });

        $this->container['helper'] = function ($c) {
            return new Rhino_Helper($c);
        };

        $this->container['queue'] = function ($c) {
            return new Rhino_Queue($c);
        };

        $this->container['db_writer'] = function ($c) {
            return  Mage::getSingleton('core/resource')->getConnection('core_write');
        };

        $this->container['db_reader'] = function ($c) {
            return Mage::getSingleton('core/resource')->getConnection('core_read');;
        };

        $this->container['db_prefix'] = function ($c) {
            return Mage::getConfig()->getTablePrefix();
        };

        $this->container['cache'] = $this->container->factory(function ($c) {
            $cache = new Cache(array(
                    'path'      => $c['base_path'].'/cache/',
                  'extension'   => '.cache',
			'name' => 'rhinoracks',
           ));

            return $cache;
        });

        $this->container['clear_cache'] = function ($c) {
            $path = $c['cache']->getCachePath();
            exec(sprintf('rm -f %s*.cache', $path));
        };
    }
}
