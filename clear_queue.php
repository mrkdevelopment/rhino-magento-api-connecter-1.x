<?php

/**
 * Script used to clear the periodic sync queue.
 */

require_once __DIR__ . '/di.php';

$container['queue']->clear();
$container['queue']->truncateIfEmpty();
$container['cache']->setCache('children_categories');
$container['cache']->eraseExpired();
