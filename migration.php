<?php

/**
 * Script that defines database migrations for the module.
 */

require_once __DIR__ . '/di.php';

$table_name = $container['db_prefix'] . 'rhino_racks_magento_queue';

$sql = 'CREATE TABLE IF NOT EXISTS `' . $table_name . '` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(15) DEFAULT NULL,
  `data` text,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `status` (`status`)
);';

$container['db_writer']->query($sql);
